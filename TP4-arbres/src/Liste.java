class Liste{
    private int val;
    private Liste suiv;
    
    //attention : utilisez plutôt "getVal()" que "val", car getVal() renvoie une exception quand la liste est vide,
	//et vous empêche donc d'utiliser la valeur "fantôme" du dernier maillon

    //liste vide =_def (*,null)
 
    //////////////////////////////////////////////
    //////// méthodes fournies
    //////////////////////////////////////////////
    
    public Liste(){
	suiv = null;
    }

    public Liste(Liste l){
	if(l.estVide()){
	    suiv=null;
	}
	else{
	    val = l.val;
	    suiv = new Liste(l.suiv);
	}
    }

    public Liste(int x, Liste l){
	val = x;
	suiv = new Liste(l);
    }

	public int getVal(){
		//sur liste non vide
		if(estVide())
			throw new RuntimeException("getVal appelée sur liste vide");
		return val;
	}
	public Liste getSuiv(){
		return suiv;
	}

	public void ajoutTete(int x){
		if(estVide()){
			val = x;
			suiv = new Liste();
		}
		else {
			Liste aux = new Liste();
			aux.val = getVal();
			aux.suiv = suiv;
			val = x;
			suiv = aux;
		}
	}

    public void supprimeTete(){
		//sur liste non vide
		if(suiv.estVide()){
			suiv = null;
		}
		else {
			val = suiv.getVal();
			suiv = suiv.suiv;
		}
    }

    public boolean estVide(){
	return suiv==null;
    }



    public String toString(){
	if(estVide()){
	    return "()";
	}
	else{
	    return getVal()+" "+suiv.toString();
	}
    }



    //////////////////////////////////////////////
    //////// méthodes du TD
    //////////////////////////////////////////////
    
	public int longueur(){
		if(estVide()){
			return 0;
		}
		else{
			return 1 + suiv.longueur();
		}
	}
    
	public int somme(){
		if(estVide()){
			return 0;
		}
		else{
			return getVal() + suiv.somme();
		}
	}

	public boolean croissant(){
		if(!suiv.estVide()){
			if(getVal() > suiv.getVal()){
				return false;
			}
			else{
				return suiv.croissant();
			}
		}
		else{
			return true;
		}	
	}

	public int get(int i){
		if(i == 0){
			return getVal();
		}
		else{
			return suiv.get(i - 1);
		}
	}

	public void ajoutFin(int x){
		if(estVide()){
			this.val = x;
			this.suiv = new Liste();
		}
		else{
			suiv.ajoutFin(x);
		}
	}

	public void concat(Liste l) {
        if (!l.estVide()) {
            if (this.estVide()) {
                this.val = l.getVal();
                this.suiv = l.getSuiv();
            } else if (this.getSuiv().estVide()) {
                suiv = l;
            } else {
                this.getSuiv().concat(l);
            }
        }
    }
	
	public Liste supprOccsV2(int x){
		if(getSuiv().estVide()){
			return new Liste();
		}
		else{
			if(getVal() == x){
				this.supprimeTete();
			}
			return suiv.supprOccsV2(x);
		}
	}

	public Liste retourne(){
		if(estVide()){
			return new Liste();
		}
		else{
			this.ajoutFin(getVal());
			this.supprimeTete();
			System.out.println(this);
			return this.retourne();
		}
	}

    public static void main(String[] arg){
		Liste l = new Liste();
		Liste m = new Liste();
		l.ajoutTete(4);
		l.ajoutTete(3);
		l.ajoutTete(2);
		l.ajoutTete(3);
		l.ajoutTete(1);
		m.ajoutTete(4);
		m.ajoutTete(5);
		m.ajoutTete(2);
		m.ajoutTete(3);
		System.out.println("l : " + l);
		System.out.println("Longueur de la liste : " + l.longueur());
		System.out.println("Somme de la liste : " + l.somme());
		System.out.println("La liste est croissante : " + l.croissant());
		System.err.println("La liste est croissante : " + m.croissant());
		System.out.println("L'élément d'indice i est : " + m.get(2));
		l.ajoutFin(6);
		System.out.println("l : " + l);
		System.out.println("m : " + m);
		l.concat(m);
		System.out.println("l : " + l);
		l.supprOccsV2(3);
		System.out.println("l : " +l);
		l.retourne();
	}
}
